// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_.h"
#include <TDS_/FuncLibrary/Types.h>
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TDS_, "TDS_" );

DEFINE_LOG_CATEGORY(LogTDS_)
 
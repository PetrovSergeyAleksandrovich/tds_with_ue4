// Fill out your copyright notice in the Description page of Project Settings.

#include <TDS_/Game/TDS_GameInstance.h>

bool UTDS_GameInstance::GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo)
{
	bool bIsFind = false;
	FWeaponInfo* WeaponInfoRow;

	if (WeaponInfoTable)
	{
		WeaponInfoRow = WeaponInfoTable->FindRow<FWeaponInfo>(NameWeapon, "", false);
		if (WeaponInfoRow)
		{
			bIsFind = true;
			OutInfo = *WeaponInfoRow;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UTDS_GameInstance::GetWeaponInfoByName - Weapon Table -NULL"));
	}


	return bIsFind;
}

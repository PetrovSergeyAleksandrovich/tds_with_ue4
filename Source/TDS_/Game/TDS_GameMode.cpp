// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_GameMode.h"
#include "TDS_PlayerController.h"
#include <TDS_/Character/TDS_Character.h>
#include "UObject/ConstructorHelpers.h"

ATDS_GameMode::ATDS_GameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATDS_PlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
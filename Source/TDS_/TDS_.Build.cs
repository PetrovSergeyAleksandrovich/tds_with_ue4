// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TDS_ : ModuleRules
{
	public TDS_(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "PhysicsCore", "HeadMountedDisplay", "NavigationSystem", "AIModule",  });
    }
}
